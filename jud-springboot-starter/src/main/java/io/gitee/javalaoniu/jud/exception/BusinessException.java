package io.gitee.javalaoniu.jud.exception;

import org.springframework.http.HttpStatus;

/**
 * @author javalaoniu
 */
public class BusinessException extends RuntimeException {

    private int code;

    public int getCode() {
        return code;
    }

    /**
     * 业务异常
     *
     * @param msg 异常消息
     */
    public BusinessException(String msg) {
        super(msg);
    }

    /**
     * 使用枚举传参
     *
     * @param httpStatus 异常枚举（spring的）
     */
    public BusinessException(HttpStatus httpStatus) {
        super(httpStatus.getReasonPhrase());
        this.code = httpStatus.value();
    }

    /**
     * 使用CommonErrorCode枚举传参
     *
     * @param exceptionCode 异常枚举
     */
    public BusinessException(ExceptionCode exceptionCode) {
        super(exceptionCode.getMessage());
        this.code = exceptionCode.getCode();
    }

    /**
     * 业务异常
     *
     * @param code code
     * @param msg  异常消息
     */
    public BusinessException(int code, String msg) {
        super(msg);
        this.code = code;
    }


    /**
     * 业务异常，异常栈详情
     *
     * @param msg  异常消息
     * @param cause 异常栈
     */
    public BusinessException(String msg, Throwable cause) {
        super(msg, cause);
    }

    /**
     * 业务异常，异常栈详情
     *
     * @param code code
     * @param msg  异常消息
     * @param cause 异常栈
     */
    public BusinessException(int code, String msg, Throwable cause) {
        super(msg, cause);
        this.code = code;
    }

}
