package io.gitee.javalaoniu.jud.exception;

import org.springframework.http.HttpStatus;

/**
 * @author javalaoniu
 */
public enum ExceptionCode {

    /**
     * 200 请求正常返回
     */
    OK(HttpStatus.OK.value(), String.format("请求正常(%s)", HttpStatus.OK.getReasonPhrase())),

    /**
     * 200 请求正常，但是没有数据
     */
    NO_CONTENT(HttpStatus.NO_CONTENT.value(),
            String.format("请求正常，但是没有数据(%s)", HttpStatus.NO_CONTENT.getReasonPhrase())),

    /**
     * 400 参数错误
     */
    PARAM_ERROR(HttpStatus.BAD_REQUEST.value(),
            String.format("参数错误(%s)", HttpStatus.BAD_REQUEST.getReasonPhrase())),

    /**
     * 401 参数错误UNAUTHORIZED(401, HttpStatus.Series.CLIENT_ERROR, "Unauthorized"),
     */
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED.value(),
            String.format("无权操作(%s)", HttpStatus.UNAUTHORIZED.getReasonPhrase())),

    /**
     * 404 Web 服务器找不到您所请求的文件或脚本。请检查URL 以确保路径正确。
     */
    NOT_FOUND(HttpStatus.NOT_FOUND.value(),
            String.format("哎呀，无法找到这个资源啦(%s)", HttpStatus.NOT_FOUND.getReasonPhrase())),

    /**
     * 405 对于请求所标识的资源，不允许使用请求行中所指定的方法。请确保为所请求的资源设置了正确的 MIME 类型。
     */
    METHOD_NOT_ALLOWED(HttpStatus.METHOD_NOT_ALLOWED.value(),
            String.format("请换个姿势操作试试(%s)", HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase())),

    /**
     * 415 Unsupported Media Type
     */
    UNSUPPORTED_MEDIA_TYPE(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(),
            String.format("哎呀，不支持该媒体类型(%s)", HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase())),

    /**
     * 429 系统限流
     */
    TRAFFIC_LIMITING(HttpStatus.TOO_MANY_REQUESTS.value(),
            String.format("哎呀，网络拥挤请稍后再试试(%s)", HttpStatus.TOO_MANY_REQUESTS.getReasonPhrase())),

    /**
     * 500 服务器的内部错误
     */
    EXCEPTION(HttpStatus.INTERNAL_SERVER_ERROR.value(),
            String.format("服务器开小差，请稍后再试(%s)", HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())),

    /**
     * 505 非法请求
     */
    ILLEGAL_REQUEST(HttpStatus.HTTP_VERSION_NOT_SUPPORTED.value(),
            String.format("非法请求(%s)", HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())),

    /**
     * 业务异常
     */
    BUSINESS_ERROR(111111, "业务异常"),

    /**
     * 未知异常
     */
    API_GATEWAY_ERROR(999999, "未知异常");

    private int code;

    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    ExceptionCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
