package io.gitee.javalaoniu.jud;

import io.gitee.javalaoniu.jud.advice.ResultAdvice;
import io.gitee.javalaoniu.jud.handler.GlobalDefaultExceptionHandler;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author javalaoniu
 */
@Configuration
@EnableConfigurationProperties(GlobalDefaultProperties.class)
@PropertySource(value = "classpath:dispose.properties", encoding = "UTF-8")
public class GlobalDefaultConfiguration {

    @Bean
    public GlobalDefaultExceptionHandler globalDefaultExceptionHandler() {
        return new GlobalDefaultExceptionHandler();
    }

    @Bean
    public ResultAdvice commonResponseDataAdvice(GlobalDefaultProperties globalDefaultProperties) {
        return new ResultAdvice(globalDefaultProperties);
    }

}
