package io.gitee.javalaoniu.jud.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.gitee.javalaoniu.jud.exception.ExceptionCode;

import java.io.Serializable;

/**
 * 统一返回对象
 * @author javalaoniu
 */
public class Result<T> implements Serializable {

    /**
     * 是否成功
     */
    private Boolean succ;

    /**
     * 服务器当前时间戳
     */
    private Long ts = System.currentTimeMillis();

    /**
     * 成功数据
     */
    private T data;

    /**
     * http 状态码
     */
    private Integer code;

    /**
     * 错误描述
     */
    private String msg;

    public Boolean getSucc() {
        return succ;
    }

    public void setSucc(Boolean succ) {
        this.succ = succ;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Result() {
    }

    public Result(Boolean succ, Long ts, T data, Integer code, String msg) {
        this.succ = succ;
        this.ts = ts;
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public static Result success() {
        Result result = new Result();
        result.succ = true;
        result.code = ExceptionCode.OK.getCode();
        return result;
    }

    public static Result success(Object data) {
        Result result = new Result();
        result.succ = true;
        result.code = ExceptionCode.OK.getCode();
        result.setData(data);
        return result;
    }

    public static Result fail(String msg) {
        Result result = new Result();
        result.succ = false;
        result.code = ExceptionCode.EXCEPTION.getCode();
        result.msg = msg;
        return result;
    }

    public static Result fail(Integer code, String msg) {
        Result result = new Result();
        result.succ = false;
        result.code = code;
        result.msg = msg;
        return result;
    }

    public static Result fail(Integer code, String msg, Object data) {
        Result result = new Result();
        result.succ = false;
        result.code = code;
        result.msg = msg;
        result.setData(data);
        return result;
    }

    public static Result fail(ExceptionCode exceptionCode) {
        Result result = new Result();
        result.succ = false;
        result.code = exceptionCode.getCode();
        result.msg = exceptionCode.getMessage();
        return result;
    }

    /**
     * 获取 json
     */
    public String buildResultJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("succ", this.succ);
        jsonObject.put("code", this.code);
        jsonObject.put("ts", this.ts);
        jsonObject.put("msg", this.msg);
        jsonObject.put("data", this.data);
        return JSON.toJSONString(jsonObject, SerializerFeature.DisableCircularReferenceDetect);
    }

    @Override
    public String toString() {
        return "Result{" +
                "succ=" + succ +
                ", ts=" + ts +
                ", data=" + data +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}