package io.gitee.javalaoniu.jud.annotation;

import io.gitee.javalaoniu.jud.GlobalDefaultConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 启用统一处理（不用注解的方式就需要在META-INF/spring.factories中配置好配置类的全路径）
 * @author javalaoniu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(GlobalDefaultConfiguration.class)
public @interface EnableUnifiedDisposal {

}
