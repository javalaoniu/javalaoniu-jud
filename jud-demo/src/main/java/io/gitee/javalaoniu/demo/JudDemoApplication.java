package io.gitee.javalaoniu.demo;

import io.gitee.javalaoniu.jud.annotation.EnableUnifiedDisposal;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableUnifiedDisposal
@SpringBootApplication
public class JudDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JudDemoApplication.class, args);
    }

}
