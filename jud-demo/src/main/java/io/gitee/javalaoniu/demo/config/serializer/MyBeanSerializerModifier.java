package io.gitee.javalaoniu.demo.config.serializer;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class MyBeanSerializerModifier extends BeanSerializerModifier {

    private JsonSerializer _nullArrayJsonSerializer = new NullArrayJsonSerializer();

    private JsonSerializer _nullStringJsonSerializer = new NullStringJsonSerializer();

    private JsonSerializer _nullIntegerJsonSerializer = new NullIntegerJsonSerializer();

    private JsonSerializer _doubleJsonSerializer = new DoubleJsonSerialize();

    @Override
    public List changeProperties(SerializationConfig config, BeanDescription beanDesc,
                                 List beanProperties) { // 循环所有的beanPropertyWriter
        for (int i = 0; i < beanProperties.size(); i++) {
            BeanPropertyWriter writer = (BeanPropertyWriter) beanProperties.get(i);
            // 判断字段的类型，如果是array，list，set则注册nullSerializer
            if (isArrayType(writer)) { //给writer注册一个自己的nullSerializer
                writer.assignNullSerializer(this.defaultNullArrayJsonSerializer());
            }
            if (isStringType(writer)) {
                writer.assignNullSerializer(this.defaultNullStringJsonSerializer());
            }
            if (isIntegerType(writer)) {
                writer.assignNullSerializer(this.defaultNullIntegerJsonSerializer());
            }
            if (isDoubleType(writer)) {
                writer.assignSerializer(this.defaultDoubleJsonSerializer());
            }
        }
        return beanProperties;
    } // 判断是什么类型

    protected boolean isArrayType(BeanPropertyWriter writer) {
        Class clazz = writer.getPropertyType();
        return clazz.isArray() || clazz.equals(List.class) || clazz.equals(Set.class);
    }

    protected boolean isStringType(BeanPropertyWriter writer) {
        Class clazz = writer.getPropertyType();
        return clazz.equals(String.class);
    }

    protected boolean isIntegerType(BeanPropertyWriter writer) {
        Class clazz = writer.getPropertyType();
        return clazz.equals(Integer.class) || clazz.equals(int.class) || clazz.equals(Long.class);
    }

    protected boolean isDoubleType(BeanPropertyWriter writer) {
        Class clazz = writer.getPropertyType();
        return clazz.equals(Double.class) || clazz.equals(BigDecimal.class);
    }


    protected JsonSerializer defaultNullArrayJsonSerializer() {
        return _nullArrayJsonSerializer;
    }

    protected JsonSerializer defaultNullStringJsonSerializer() {
        return _nullStringJsonSerializer;
    }

    protected JsonSerializer defaultNullIntegerJsonSerializer() {
        return _nullIntegerJsonSerializer;
    }

    protected JsonSerializer defaultDoubleJsonSerializer() {
        return _doubleJsonSerializer;
    }
}
