package io.gitee.javalaoniu.demo.config.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.DecimalFormat;

public class DoubleJsonSerialize extends JsonSerializer {
    private DecimalFormat df = new DecimalFormat("##.00");

    @Override
    public void serialize(Object value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (value != null) {
            jsonGenerator.writeString(df.format(value));
        }else{
            jsonGenerator.writeString("0.00");
        }

    }
}

