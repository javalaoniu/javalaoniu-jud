package io.gitee.javalaoniu.demo.controller;

import io.gitee.javalaoniu.jud.annotation.IgnoreResponseAdvice;
import io.gitee.javalaoniu.jud.common.Result;
import io.gitee.javalaoniu.jud.exception.BusinessException;
import io.gitee.javalaoniu.jud.exception.ExceptionCode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DemoController {

    @GetMapping("test1")
    public String stringTest() {
        return "hello";
    }

    @GetMapping("test2")
    public String stringNullTest() {
        return null;
    }

    @GetMapping("test3")
    public Object objectEntityTest() {
        DemoEntity demoEntity = new DemoEntity();
        demoEntity.setName("张三");
        demoEntity.setAge(50);
        demoEntity.setSex(false);
        demoEntity.setSalary(4500000001542.26);
        return demoEntity;
    }

    @GetMapping("test4")
    public Object objectNotNullTest() {
        return "hello Object";
    }

    @GetMapping("test5")
    public Object objectNullTest() {
        return null;
    }

    @GetMapping("test6")
    public List<DemoEntity> listTest() {
        DemoEntity demoEntity2 = new DemoEntity();
        demoEntity2.setName("张三");
        demoEntity2.setAge(50);
        demoEntity2.setSex(false);
        demoEntity2.setSalary(4500000001542.26);

        DemoEntity demoEntity = new DemoEntity();
        demoEntity.setName("张三");
        demoEntity.setAge(50);
        demoEntity.setSex(false);
        demoEntity.setSalary(4500000001542.26);

        List<DemoEntity> list = new ArrayList<>();
        list.add(demoEntity);
        list.add(demoEntity2);

        return list;
    }

    @GetMapping("test7")
    public List<String> listNullTest() {
        return null;
    }

    @GetMapping("test8")
    public Result resultTest() {
        DemoEntity demoEntity = new DemoEntity();
        demoEntity.setName("张三");
        demoEntity.setAge(50);
        demoEntity.setSex(false);
        demoEntity.setSalary(4500000001542.2656564545);
        return Result.success(demoEntity);
    }

    @IgnoreResponseAdvice
    @GetMapping("test9")
    public String ignoreResponseTest() {
        return "IgnoreResponseAdvice";
    }

    @GetMapping("test10")
    public String businessExceptionTest() {
        throw new BusinessException(ExceptionCode.EXCEPTION);
    }
}