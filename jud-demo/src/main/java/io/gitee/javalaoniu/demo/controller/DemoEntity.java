package io.gitee.javalaoniu.demo.controller;

import lombok.Data;

@Data
public class DemoEntity {
    private String name;
    private Integer age;
    private Boolean sex;
    private Double salary;
}
