# Java老牛项目之统一处理starter
统一处理系统返回对象和异常统一捕获

`jud为javalaoniu unified disposal的简称`

## 添加依赖
添加统一处理依赖
```xml
<dependency>  
    <groupId>io.gitee.javalaoniu</groupId>  
    <artifactId>jud-springboot-starter</artifactId>  
    <version>0.0.1</version>  
</dependency>
```

## 启用统一处理
添加 @EnableUnifiedDisposal 注解
```java
import io.gitee.javalaoniu.jud.annotation.EnableUnifiedDisposal;  
import org.springframework.boot.SpringApplication;  
import org.springframework.boot.autoconfigure.SpringBootApplication;  
  
@EnableUnifiedDisposal  
@SpringBootApplication  
public class JudDemoApplication {  
    public static void main(String[] args) {  
        SpringApplication.run(JudDemoApplication.class, args);  
    }  
}
```


## 拦截的处理
像平常一样返回数据即可，不需要做其它
```java
import io.gitee.javalaoniu.jud.annotation.IgnoreResponseAdvice;  
import io.gitee.javalaoniu.jud.common.Result;  
import io.gitee.javalaoniu.jud.exception.BusinessException;  
import io.gitee.javalaoniu.jud.exception.ExceptionCode;  
import org.springframework.web.bind.annotation.GetMapping;  
import org.springframework.web.bind.annotation.RestController;  
  
import java.util.ArrayList;  
import java.util.List;  
  
@RestController  
public class DemoController {  
  
    @GetMapping("test1")  
    public String stringTest() {  
        return "hello";  
        // {"code":200,"data":"hello","succ":true,"ts":1673943672244}
    }  
  
    @GetMapping("test2")  
    public String stringNullTest() {  
        return null;  
        // {"code":200,"data":"","succ":true,"ts":1673943691844}
    }  
  
    @GetMapping("test3")  
    public Object objectEntityTest() {  
        DemoEntity demoEntity = new DemoEntity();  
        demoEntity.setName("张三");  
        demoEntity.setAge(50);  
        demoEntity.setSex(false);  
        demoEntity.setSalary(4500000001542.26);  
        return demoEntity;  
        // {"succ":true,"ts":1673943709119,"data":{"name":"张三","age":50,"sex":false,"salary":4.50000000154226E12},"code":200,"msg":null}
    }  
  
    @GetMapping("test4")  
    public Object objectNotNullTest() {  
        return "hello Object";  
        // {"code":200,"data":"hello Object","succ":true,"ts":1673943726435}
    }  
  
    @GetMapping("test5")  
    public Object objectNullTest() {  
        return null;  
        // 啥也没返回，但是如果配置了json转换器的话会返回：{"code":200,"data":null,"succ":true,"ts":1673943726435}
    }  
  
    @GetMapping("test6")  
    public List<DemoEntity> listTest() {  
        DemoEntity demoEntity2 = new DemoEntity();  
        demoEntity2.setName("张三");  
        demoEntity2.setAge(50);  
        demoEntity2.setSex(false);  
        demoEntity2.setSalary(4500000001542.26);  
  
        DemoEntity demoEntity = new DemoEntity();  
        demoEntity.setName("张三");  
        demoEntity.setAge(50);  
        demoEntity.setSex(false);  
        demoEntity.setSalary(4500000001542.26);  
  
        List<DemoEntity> list = new ArrayList<>();  
        list.add(demoEntity);  
        list.add(demoEntity2);  
  
        return list;  
        // {"succ":true,"ts":1673943797079,"data":[{"name":"张三","age":50,"sex":false,"salary":4.50000000154226E12},{"name":"张三","age":50,"sex":false,"salary":4.50000000154226E12}],"code":200,"msg":null}
    }  
  
    @GetMapping("test7")  
    public List<String> listNullTest() {  
        return null;  
        // {"succ":true,"ts":1673943819382,"data":null,"code":200,"msg":null}
    }  
  
    @GetMapping("test8")  
    public Result resultTest() {  
        DemoEntity demoEntity = new DemoEntity();  
        demoEntity.setName("张三");  
        demoEntity.setAge(50);  
        demoEntity.setSex(false);  
        demoEntity.setSalary(4500000001542.2656564545);  
        return Result.success(demoEntity);  
        // {"succ":true,"ts":1673943832081,"data":{"name":"张三","age":50,"sex":false,"salary":4.500000001542266E12},"code":200,"msg":null}
    }  
  
    @IgnoreResponseAdvice  
    @GetMapping("test9")  
    public String ignoreResponseTest() {  
        return "IgnoreResponseAdvice";  
        // IgnoreResponseAdvice
    }  
  
    @GetMapping("test10")  
    public String businessExceptionTest() {  
        throw new BusinessException(ExceptionCode.EXCEPTION);  
        // {"succ":false,"ts":1673943862588,"data":null,"code":500,"msg":"服务器开小差，请稍后再试(Internal Server Error)"}
    }  
}
```

## 不拦截处理
对不需要统一处理的controller或者方法使用下面注解
```java
@IgnoreResponseAdvice  
@GetMapping("test9")  
public String ignoreResponseTest() {  
	// 在方法上使用，直接返回IgnoreResponseAdvice字符串给前端
    return "IgnoreResponseAdvice";  
}
```

